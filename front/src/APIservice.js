import axios from 'axios';

const CLIENT = axios.create({
  baseURL: 'http://localhost:8888/rest/v1/',
  timeout: 10000,
});
export default {
  name: 'APIService',
  getAllTags() {
    return CLIENT.get('tags');
  },
  getNews(query) {
    return CLIENT.get('news/search', query);
  },
};
