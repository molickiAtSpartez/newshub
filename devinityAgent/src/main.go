package main

import (
	"encoding/json"
	"fmt"
	"github.com/go-stomp/stomp"
	"github.com/robfig/cron"
	"gopkg.in/resty.v1"
	"log"
	"math"
	"net/http"
	"os"
	"regexp"
	"strconv"
)

const ApiHost = "https://deviniti.com/wp-json/wp/v2/"
const Posts = ApiHost + "posts"

type Title struct {
	Rendered string `json:"rendered"`
}

type Content struct {
	Rendered string `json:"rendered"`
}

type Post struct {
	Id      int     `json:"id"`
	Date    string  `json:"date_gmt"`
	Title   Title   `json:"title"`
	Link    string  `json:"link"`
	Content Content `json:"content"`
	Img     string  `json:"img"`
	Tag     string  `json:"tag"`
}

func getTotalPosts() int {
	resp, err := resty.R().Get(Posts)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	total, _ := strconv.Atoi(resp.Header().Get("X-Wp-Total"))
	return total
}

func getPage(page int) *resty.Response {
	resp, _ := resty.R().SetQueryParam("per_page", "10").
		SetQueryParam("page", strconv.Itoa(page)).
		Get(Posts)
	return resp
}

func parseImg(posts *[]Post) {
	imgTagRegex, _ := regexp.Compile("<img.*\\/>")
	imgSrcRegex, _ := regexp.Compile("src=\"[^\"]*\"")
	for index, post := range *posts {
		imgTag := imgTagRegex.FindString(post.Content.Rendered)
		imgSrc := imgSrcRegex.FindString(imgTag)
		imgSrc = imgSrc[5 : len(imgSrc)-1]
		(*posts)[index].Img = imgSrc
	}
}

func tagPosts(posts *[]Post) {
	for _, post := range *posts {
		post.Tag = "DEVINITY"
	}
}

func responseBodyToJsonObject(response *resty.Response) (posts []Post) {
	_ = json.Unmarshal(response.Body(), &posts)
	parseImg(&posts)
	tagPosts(&posts)
	return
}

func getPostsForPage(page int) []Post {
	response := getPage(page)
	return responseBodyToJsonObject(response)
}

func putPostsToQueue(posts []Post) {
	c, err := stomp.Dial("tcp", "activemq:61613")
	if err != nil {
		fmt.Errorf("Error", err)
	}
	fmt.Println("Sending data to queue")
	fmt.Println("Posts: ", len(posts))
	for _, post := range posts {
		postBytes, err := json.Marshal(post)
		if err != nil {
			fmt.Println("Could not marshal post of id: ", post.Id)
		}
		fmt.Println("Sending to topic: ", post.Id)
		err = c.Send("posts", "application/json", postBytes)
		if err != nil {
			fmt.Println("Could not send post of id: ", post.Id)
		}
	}
	_ = c.Disconnect()
}

func pullPosts() {
	total := getTotalPosts()
	pages := math.Ceil(float64(total) / 10.0)
	for i := 1; i < int(pages); i++ {
		posts := getPostsForPage(i)
		putPostsToQueue(posts)
	}
}

func stopCron(w http.ResponseWriter, r *http.Request, cron *cron.Cron) {
	if r.Method == "POST" {
		log.Println("Stopping CRON by request from", r.RemoteAddr, r.Host)
		cron.Stop()
		w.WriteHeader(200)
		w.Write([]byte("CRON agent stopped2"))
	}
}

func startCron(w http.ResponseWriter, r *http.Request, cron *cron.Cron) {
	if r.Method == "POST" {
		log.Println("Starting CRON by request from", r.RemoteAddr, r.Host)
		cron.Start()
		w.WriteHeader(200)
		w.Write([]byte("CRON agent started2"))
	}
}

func forcePull(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		pullPosts()
		w.Write([]byte("DONE"))
	}
}

func main() {
	c := cron.New()
	_ = c.AddFunc("@hourly", pullPosts)
	c.Start()
	http.HandleFunc("/off", func(writer http.ResponseWriter, request *http.Request) {
		stopCron(writer, request, c)
	})
	http.HandleFunc("/on", func(writer http.ResponseWriter, request *http.Request) {
		startCron(writer, request, c)
	})
	http.HandleFunc("/tick", forcePull)
	_ = http.ListenAndServe(":8083", nil)
}
