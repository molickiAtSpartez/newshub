package main

import (
	"fmt"
	"github.com/go-stomp/stomp"
)

func pullMessage() {
	conn, err := stomp.Dial("tcp", "localhost:61613")
	if err != nil {
		fmt.Errorf("Err", err)
	}

	sub, err := conn.Subscribe("/topic/posts", stomp.AckClient)
	if err != nil {
		fmt.Errorf("Err", err)
	}

	// receive 5 messages and then quit
	for i := 0; i < 5000; i++ {
		msg := <-sub.C
		if msg.Err != nil {
			fmt.Errorf("Err", msg.Err)
		}
		// acknowledge the message
		fmt.Println("Got msg: ", string(msg.Body))
		err = conn.Ack(msg)
		if err != nil {
			fmt.Errorf("Err", err)
		}
	}

	err = sub.Unsubscribe()
	if err != nil {
		fmt.Errorf("Err", err)
	}
	conn.Disconnect()
}

func main() {
	pullMessage()
}