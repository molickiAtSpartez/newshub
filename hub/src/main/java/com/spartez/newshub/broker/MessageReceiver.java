package com.spartez.newshub.broker;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spartez.newshub.aggregate.news.News;
import com.spartez.newshub.aggregate.news.NewsRepository;
import com.spartez.newshub.aggregate.tags.Tags;
import com.spartez.newshub.wp.Post;
import org.apache.activemq.command.ActiveMQBytesMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.util.StringUtils;

import javax.jms.MessageListener;

@Component
public class MessageReceiver implements MessageListener {

    private final NewsRepository newsRepository;
    private final Tags tags;
    private static final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public MessageReceiver(NewsRepository newsRepository, Tags tags) {
        this.newsRepository = newsRepository;
        this.tags = tags;
    }

    private News createNews(Post post) {
        News news = new News();
        news.setDate(post.getDate());
        news.setExternalUrl(post.getLink());
        news.setImgUrl(post.getImg());
        news.setTitle(post.getTitle().getRendered());
        news.setTag(tags.getBySource("DEVINITY")
                        .orElse(tags.createTag("DEVINITY")));
        return news;
    }

    private Post deserializePost(String postJson) {
        try {
            return mapper.readValue(postJson, Post.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onMessage(javax.jms.Message message) {
        String postJson = new String(((ActiveMQBytesMessage) message).getContent().data);
        Post post = deserializePost(postJson);
        StringUtils.unescapeJavaScript(post.getTitle().getRendered());
        newsRepository.save(createNews(post));
    }
}
