package com.spartez.newshub.broker;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.listener.MessageListenerContainer;
import org.springframework.jms.support.converter.MessageConverter;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class MessagingConfiguration {
    private static final Logger log = LoggerFactory.getLogger(MessagingConfiguration.class);
    private static String DEFAULT_BROKER_URL = "tcp://localhost:61616";
    private static final String POSTS_QUEUE = "posts";

    private final ActiveMQConnectionFactory connectionFactory;

    @Autowired
    public MessagingConfiguration(@Qualifier("amqConnectionFactory") ActiveMQConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Bean
    public JmsTemplate jmsTemplate() {
        PostTemplate postTemplate = new PostTemplate();
        postTemplate.setConnectionFactory(connectionFactory);
        postTemplate.setDefaultDestinationName(POSTS_QUEUE);
        postTemplate.setMessageConverter(new PostConverter());
        postTemplate.setDestinationResolver(new PostResolver());
        return postTemplate;
    }

    @Bean
    public MessageListenerContainer getContainer(MessageReceiver messageReceiver) {
        DefaultMessageListenerContainer container = new DefaultMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setDestinationName(POSTS_QUEUE);
        container.setMessageListener(messageReceiver);
        return container;
    }

    @Bean
    public MessageConverter messageConverter() {
        return new PostConverter();
    }
}
