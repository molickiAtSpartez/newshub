package com.spartez.newshub.broker;

import org.apache.activemq.openwire.v3.ActiveMQBlobMessageMarshaller;
import org.apache.activemq.openwire.v3.ActiveMQBytesMessageMarshaller;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.Destination;

public class PostTemplate extends JmsTemplate {

    @Override
    public Object receiveAndConvert() throws JmsException {
        return super.receiveAndConvert();
    }

    @Override
    public Object receiveAndConvert(Destination destination) throws JmsException {
        return super.receiveAndConvert(destination);
    }

    @Override
    public Object receiveAndConvert(String destinationName) throws JmsException {
        return super.receiveAndConvert(destinationName);
    }
}
