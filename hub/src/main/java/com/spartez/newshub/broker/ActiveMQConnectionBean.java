package com.spartez.newshub.broker;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:/application-docker.properties")
public class ActiveMQConnectionBean {

    @Bean("amqConnectionFactory")
    public ActiveMQConnectionFactory connectionFactory(@Value("${activemq.host}") String activeMQHost) {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL(activeMQHost);
        return connectionFactory;
    }
}
