package com.spartez.newshub.aggregate.news;

import com.spartez.newshub.aggregate.tags.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NewsRepository extends CrudRepository<News, Integer> {

    Page<News> findNewsByTagIn(List<Tag> tags, Pageable page);
    Page<News> findBy(Pageable page);

}
