package com.spartez.newshub.aggregate.news;

public class NewsDto {

    private final String title;
    private final String url;
    private final String img;
    private final String date;
    private final String tag;

    public NewsDto(String title, String url, String img, String date, String tag) {
        this.title = title;
        this.url = url;
        this.img = img;
        this.date = date;
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getImg() {
        return img;
    }

    public String getDate() {
        return date;
    }

    public String getTag() {
        return tag;
    }
}
