package com.spartez.newshub.aggregate.tags;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class Tags {

    private final TagsRepository tagsRepository;

    @Autowired
    public Tags(TagsRepository tagsRepository) {
        this.tagsRepository = tagsRepository;
    }

    public Tag createTag(String source) {
        return getBySource(source)
                .orElseGet(() -> {
                    Tag newTag = new Tag();
                    newTag.setSource(source);
                    return tagsRepository.save(newTag);
                });
    }

    public Optional<Tag> getBySource(String source) {
        return tagsRepository.findTagBySourceIgnoreCase(source);
    }
}
