package com.spartez.newshub.aggregate.news;

import com.spartez.newshub.aggregate.tags.Tag;
import com.spartez.newshub.aggregate.tags.Tags;
import io.micrometer.core.instrument.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.util.StreamUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/rest/v1/news")
public class NewsResource {

    private final NewsRepository newsRepository;
    private final Tags tags;
    private final NewsToDto newsToDto;

    @Autowired
    public NewsResource(NewsRepository newsRepository, Tags tags) {
        this.newsRepository = newsRepository;
        this.tags = tags;
        this.newsToDto = new NewsToDto<>();
    }

    private Sort prepareSort(NewsQuery newsQuery) {
        if (StringUtils.isNotBlank(newsQuery.getSortField())) {
            return Sort.by(newsQuery.getAsc() ? Sort.Direction.ASC : Sort.Direction.DESC,
                           newsQuery.getSortField());
        } else {
            return Sort.unsorted();
        }
    }

    private int getLimitOrMaxAllowedSize(NewsQuery newsQuery) {
        return Math.min(newsQuery.getLimit(), 100);
    }

    private PageRequest preparePageable(NewsQuery newsQuery) {
        return PageRequest.of(
                newsQuery.getPage(),
                getLimitOrMaxAllowedSize(newsQuery),
                prepareSort(newsQuery)
                             );
    }

    @GetMapping("/search")
    public Page<NewsDto> findNews(@Valid NewsQuery newsQuery) {
        if (newsQuery.getSources() == null || newsQuery.getSources().length == 0) {
            return newsRepository.findBy(preparePageable(newsQuery))
                                 .map(newsToDto);
        } else {
            return newsRepository.findNewsByTagIn(sourcesQueryAsTags(newsQuery), preparePageable(newsQuery))
                                 .map(newsToDto);
        }
    }
  
    @GetMapping("")
    public List<NewsDto> getAllNews() {
        return StreamUtils.createStreamFromIterator(newsRepository.findAll().iterator())
                          .map(this::unescapeTitle)
                          .map(newsToDto::apply)
                          .collect(Collectors.toList());
    }


    private News unescapeTitle(News news) {
        news.setTitle(unescapeCharacters(news.getTitle()));
        return news;
    }

    private String unescapeCharacters(String title) {
        return title.replaceAll("&#8211;", "-")
                    .replaceAll("&#8217;", "`");
    }

    private List<Tag> sourcesQueryAsTags(NewsQuery newsQuery) {
        return Arrays.stream(newsQuery.getSources())
                     .map(tags::getBySource)
                     .filter(Optional::isPresent)
                     .map(Optional::get)
                     .collect(Collectors.toList());
    }
}
