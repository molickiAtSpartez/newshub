package com.spartez.newshub.aggregate.tags;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/rest/v1/tags")
public class TagsResource {
    private final TagsRepository tagsRepository;

    @Autowired
    public TagsResource(TagsRepository tagsRepository) {
        this.tagsRepository = tagsRepository;
    }


    @GetMapping
    public Set<TagDto> getAllTags() {
        return StreamSupport.stream(tagsRepository.findAll().spliterator(), false)
                            .map(t -> new TagDto(t.getSource()))
                            .collect(Collectors.toSet());
    }
}
