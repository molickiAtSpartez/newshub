package com.spartez.newshub.aggregate.news;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

public class NewsQuery {

    private String sortField;
    private Boolean asc;
    private String[] sources;
    @PositiveOrZero
    private Integer page;
    @Positive
    private Integer limit;

    public NewsQuery() {
        this.asc = true;
        this.page = 0;
        this.limit = 50;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public Boolean getAsc() {
        return asc;
    }

    public void setAsc(Boolean asc) {
        this.asc = asc;
    }

    public String[] getSources() {
        return sources;
    }

    public void setSources(String[] sources) {
        this.sources = sources;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}
