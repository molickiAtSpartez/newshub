package com.spartez.newshub.aggregate.tags;

public class TagDto {

    public final String source;

    public TagDto(String source) {
        this.source = source;
    }
}
