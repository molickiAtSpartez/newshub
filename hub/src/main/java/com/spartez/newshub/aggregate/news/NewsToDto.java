package com.spartez.newshub.aggregate.news;


import java.util.function.Function;

public class NewsToDto<T extends News, R extends NewsDto> implements Function<News, NewsDto> {
    @Override
    public NewsDto apply(News news) {
        return new NewsDto(news.getTitle(),
                           news.getExternalUrl(),
                           news.getImgUrl(),
                           news.getDate(),
                           news.getTag().getSource());
    }
}
