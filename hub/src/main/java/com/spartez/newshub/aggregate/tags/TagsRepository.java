package com.spartez.newshub.aggregate.tags;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TagsRepository extends CrudRepository<Tag, Integer> {
    Optional<Tag> findTagBySourceIgnoreCase(String source);
}
