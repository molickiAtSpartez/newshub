package com.spartez.newshub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewsHub {

    public static void main(String[] args) {
        SpringApplication.run(NewsHub.class, args);
    }
}
