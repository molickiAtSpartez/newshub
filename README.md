# News Hub

## Hub development
- To run full environment just use `start.sh` script. Hub will be available on `8888` port.
- Otherwise, if you want to develop `hub` with complete setup, then use `start-standalone.sh` script. Hub will be available on 8082 port once you run it.

## GoLang Agent
- In order to stop cron task on agent, one must perform POST request on `{host}:8083/off`
- Simultaneously, request to `{host}:8083/on` will turn on the cron agent
