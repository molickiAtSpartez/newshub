#!/bin/bash
#### build hub backend
cd hub &&
mvn package &&
mvn install dockerfile:build

#### build devinity golang agent
cd ../devinityAgent && docker build -t news-hub/devinity .

#### run this thing... 
cd ../docker &&
docker-compose up &
