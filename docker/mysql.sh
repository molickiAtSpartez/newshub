#!/bin/bash
docker run --name mysql \
-e MYSQL_ROOT_PASSWORD=mysql \
-e MYSQL_USER=mysql \
-e MYSQL_PASSWORD=mysql \
-e MYSQL_DATABASE=hub \
-p 4306:3306 -d mysql:latest
