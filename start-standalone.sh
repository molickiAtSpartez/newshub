#!/bin/bash
#### build hub backend
cd hub &&
mvn package -q &&
mvn install -q dockerfile:build

#### build devinity golang agent
cd ../devinityAgent &&
	yes | docker build -t news-hub/devinity .

#### run this thing... 
cd ../docker &&
docker-compose -f docker-compose-standalone.yml up &
